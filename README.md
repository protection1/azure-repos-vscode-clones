<!DOCTYPE html>
<html lang="en">
  
  
  
  
  
  <head>
    <meta id="bb-bootstrap" data-current-user="{&quot;isKbdShortcutsEnabled&quot;: true, &quot;isSshEnabled&quot;: false, &quot;isAuthenticated&quot;: false}"
 />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bitbucket</title>
    <script nonce="npUVzrfhXAh4b0si" type="text/javascript">(window.NREUM||(NREUM={})).loader_config={xpid:"VwMGVVZSGwIIUFBQDwU="};window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var o=e[n]={exports:{}};t[n][0].call(o.exports,function(e){var o=t[n][1][e];return r(o||e)},o,o.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(t,e,n){function r(t){try{s.console&&console.log(t)}catch(e){}}var o,i=t("ee"),a=t(23),s={};try{o=localStorage.getItem("__nr_flags").split(","),console&&"function"==typeof console.log&&(s.console=!0,o.indexOf("dev")!==-1&&(s.dev=!0),o.indexOf("nr_dev")!==-1&&(s.nrDev=!0))}catch(c){}s.nrDev&&i.on("internal-error",function(t){r(t.stack)}),s.dev&&i.on("fn-err",function(t,e,n){r(n.stack)}),s.dev&&(r("NR AGENT IN DEVELOPMENT MODE"),r("flags: "+a(s,function(t,e){return t}).join(", ")))},{}],2:[function(t,e,n){function r(t,e,n,r,s){try{l?l-=1:o(s||new UncaughtException(t,e,n),!0)}catch(f){try{i("ierr",[f,c.now(),!0])}catch(d){}}return"function"==typeof u&&u.apply(this,a(arguments))}function UncaughtException(t,e,n){this.message=t||"Uncaught error with no additional information",this.sourceURL=e,this.line=n}function o(t,e){var n=e?null:c.now();i("err",[t,n])}var i=t("handle"),a=t(24),s=t("ee"),c=t("loader"),f=t("gos"),u=window.onerror,d=!1,p="nr@seenError",l=0;c.features.err=!0,t(1),window.onerror=r;try{throw new Error}catch(h){"stack"in h&&(t(13),t(12),"addEventListener"in window&&t(6),c.xhrWrappable&&t(14),d=!0)}s.on("fn-start",function(t,e,n){d&&(l+=1)}),s.on("fn-err",function(t,e,n){d&&!n[p]&&(f(n,p,function(){return!0}),this.thrown=!0,o(n))}),s.on("fn-end",function(){d&&!this.thrown&&l>0&&(l-=1)}),s.on("internal-error",function(t){i("ierr",[t,c.now(),!0])})},{}],3:[function(t,e,n){t("loader").features.ins=!0},{}],4:[function(t,e,n){function r(){j++,L=y.hash,this[u]=x.now()}function o(){j--,y.hash!==L&&i(0,!0);var t=x.now();this[h]=~~this[h]+t-this[u],this[d]=t}function i(t,e){E.emit("newURL",[""+y,e])}function a(t,e){t.on(e,function(){this[e]=x.now()})}var s="-start",c="-end",f="-body",u="fn"+s,d="fn"+c,p="cb"+s,l="cb"+c,h="jsTime",m="fetch",v="addEventListener",w=window,y=w.location,x=t("loader");if(w[v]&&x.xhrWrappable){var g=t(10),b=t(11),E=t(8),R=t(6),O=t(13),C=t(7),P=t(14),T=t(9),N=t("ee"),S=N.get("tracer");t(16),x.features.spa=!0;var L,j=0;N.on(u,r),N.on(p,r),N.on(d,o),N.on(l,o),N.buffer([u,d,"xhr-done","xhr-resolved"]),R.buffer([u]),O.buffer(["setTimeout"+c,"clearTimeout"+s,u]),P.buffer([u,"new-xhr","send-xhr"+s]),C.buffer([m+s,m+"-done",m+f+s,m+f+c]),E.buffer(["newURL"]),g.buffer([u]),b.buffer(["propagate",p,l,"executor-err","resolve"+s]),S.buffer([u,"no-"+u]),T.buffer(["new-jsonp","cb-start","jsonp-error","jsonp-end"]),a(P,"send-xhr"+s),a(N,"xhr-resolved"),a(N,"xhr-done"),a(C,m+s),a(C,m+"-done"),a(T,"new-jsonp"),a(T,"jsonp-end"),a(T,"cb-start"),E.on("pushState-end",i),E.on("replaceState-end",i),w[v]("hashchange",i,!0),w[v]("load",i,!0),w[v]("popstate",function(){i(0,j>1)},!0)}},{}],5:[function(t,e,n){function r(t){}if(window.performance&&window.performance.timing&&window.performance.getEntriesByType){var o=t("ee"),i=t("handle"),a=t(13),s=t(12),c="learResourceTimings",f="addEventListener",u="resourcetimingbufferfull",d="bstResource",p="resource",l="-start",h="-end",m="fn"+l,v="fn"+h,w="bstTimer",y="pushState",x=t("loader");x.features.stn=!0,t(8);var g=NREUM.o.EV;o.on(m,function(t,e){var n=t[0];n instanceof g&&(this.bstStart=x.now())}),o.on(v,function(t,e){var n=t[0];n instanceof g&&i("bst",[n,e,this.bstStart,x.now()])}),a.on(m,function(t,e,n){this.bstStart=x.now(),this.bstType=n}),a.on(v,function(t,e){i(w,[e,this.bstStart,x.now(),this.bstType])}),s.on(m,function(){this.bstStart=x.now()}),s.on(v,function(t,e){i(w,[e,this.bstStart,x.now(),"requestAnimationFrame"])}),o.on(y+l,function(t){this.time=x.now(),this.startPath=location.pathname+location.hash}),o.on(y+h,function(t){i("bstHist",[location.pathname+location.hash,this.startPath,this.time])}),f in window.performance&&(window.performance["c"+c]?window.performance[f](u,function(t){i(d,[window.performance.getEntriesByType(p)]),window.performance["c"+c]()},!1):window.performance[f]("webkit"+u,function(t){i(d,[window.performance.getEntriesByType(p)]),window.performance["webkitC"+c]()},!1)),document[f]("scroll",r,{passive:!0}),document[f]("keypress",r,!1),document[f]("click",r,!1)}},{}],6:[function(t,e,n){function r(t){for(var e=t;e&&!e.hasOwnProperty(u);)e=Object.getPrototypeOf(e);e&&o(e)}function o(t){s.inPlace(t,[u,d],"-",i)}function i(t,e){return t[1]}var a=t("ee").get("events"),s=t(26)(a,!0),c=t("gos"),f=XMLHttpRequest,u="addEventListener",d="removeEventListener";e.exports=a,"getPrototypeOf"in Object?(r(document),r(window),r(f.prototype)):f.prototype.hasOwnProperty(u)&&(o(window),o(f.prototype)),a.on(u+"-start",function(t,e){var n=t[1],r=c(n,"nr@wrapped",function(){function t(){if("function"==typeof n.handleEvent)return n.handleEvent.apply(n,arguments)}var e={object:t,"function":n}[typeof n];return e?s(e,"fn-",null,e.name||"anonymous"):n});this.wrapped=t[1]=r}),a.on(d+"-start",function(t){t[1]=this.wrapped||t[1]})},{}],7:[function(t,e,n){function r(t,e,n){var r=t[e];"function"==typeof r&&(t[e]=function(){var t=r.apply(this,arguments);return o.emit(n+"start",arguments,t),t.then(function(e){return o.emit(n+"end",[null,e],t),e},function(e){throw o.emit(n+"end",[e],t),e})})}var o=t("ee").get("fetch"),i=t(23);e.exports=o;var a=window,s="fetch-",c=s+"body-",f=["arrayBuffer","blob","json","text","formData"],u=a.Request,d=a.Response,p=a.fetch,l="prototype";u&&d&&p&&(i(f,function(t,e){r(u[l],e,c),r(d[l],e,c)}),r(a,"fetch",s),o.on(s+"end",function(t,e){var n=this;if(e){var r=e.headers.get("content-length");null!==r&&(n.rxSize=r),o.emit(s+"done",[null,e],n)}else o.emit(s+"done",[t],n)}))},{}],8:[function(t,e,n){var r=t("ee").get("history"),o=t(26)(r);e.exports=r;var i=window.history&&window.history.constructor&&window.history.constructor.prototype,a=window.history;i&&i.pushState&&i.replaceState&&(a=i),o.inPlace(a,["pushState","replaceState"],"-")},{}],9:[function(t,e,n){function r(t){function e(){c.emit("jsonp-end",[],p),t.removeEventListener("load",e,!1),t.removeEventListener("error",n,!1)}function n(){c.emit("jsonp-error",[],p),c.emit("jsonp-end",[],p),t.removeEventListener("load",e,!1),t.removeEventListener("error",n,!1)}var r=t&&"string"==typeof t.nodeName&&"script"===t.nodeName.toLowerCase();if(r){var o="function"==typeof t.addEventListener;if(o){var a=i(t.src);if(a){var u=s(a),d="function"==typeof u.parent[u.key];if(d){var p={};f.inPlace(u.parent,[u.key],"cb-",p),t.addEventListener("load",e,!1),t.addEventListener("error",n,!1),c.emit("new-jsonp",[t.src],p)}}}}}function o(){return"addEventListener"in window}function i(t){var e=t.match(u);return e?e[1]:null}function a(t,e){var n=t.match(p),r=n[1],o=n[3];return o?a(o,e[r]):e[r]}function s(t){var e=t.match(d);return e&&e.length>=3?{key:e[2],parent:a(e[1],window)}:{key:t,parent:window}}var c=t("ee").get("jsonp"),f=t(26)(c);if(e.exports=c,o()){var u=/[?&](?:callback|cb)=([^&#]+)/,d=/(.*)\.([^.]+)/,p=/^(\w+)(\.|$)(.*)$/,l=["appendChild","insertBefore","replaceChild"];Node&&Node.prototype&&Node.prototype.appendChild?f.inPlace(Node.prototype,l,"dom-"):(f.inPlace(HTMLElement.prototype,l,"dom-"),f.inPlace(HTMLHeadElement.prototype,l,"dom-"),f.inPlace(HTMLBodyElement.prototype,l,"dom-")),c.on("dom-start",function(t){r(t[0])})}},{}],10:[function(t,e,n){var r=t("ee").get("mutation"),o=t(26)(r),i=NREUM.o.MO;e.exports=r,i&&(window.MutationObserver=function(t){return this instanceof i?new i(o(t,"fn-")):i.apply(this,arguments)},MutationObserver.prototype=i.prototype)},{}],11:[function(t,e,n){function r(t){var e=a.context(),n=s(t,"executor-",e),r=new f(n);return a.context(r).getCtx=function(){return e},a.emit("new-promise",[r,e],e),r}function o(t,e){return e}var i=t(26),a=t("ee").get("promise"),s=i(a),c=t(23),f=NREUM.o.PR;e.exports=a,f&&(window.Promise=r,["all","race"].forEach(function(t){var e=f[t];f[t]=function(n){function r(t){return function(){a.emit("propagate",[null,!o],i),o=o||!t}}var o=!1;c(n,function(e,n){Promise.resolve(n).then(r("all"===t),r(!1))});var i=e.apply(f,arguments),s=f.resolve(i);return s}}),["resolve","reject"].forEach(function(t){var e=f[t];f[t]=function(t){var n=e.apply(f,arguments);return t!==n&&a.emit("propagate",[t,!0],n),n}}),f.prototype["catch"]=function(t){return this.then(null,t)},f.prototype=Object.create(f.prototype,{constructor:{value:r}}),c(Object.getOwnPropertyNames(f),function(t,e){try{r[e]=f[e]}catch(n){}}),a.on("executor-start",function(t){t[0]=s(t[0],"resolve-",this),t[1]=s(t[1],"resolve-",this)}),a.on("executor-err",function(t,e,n){t[1](n)}),s.inPlace(f.prototype,["then"],"then-",o),a.on("then-start",function(t,e){this.promise=e,t[0]=s(t[0],"cb-",this),t[1]=s(t[1],"cb-",this)}),a.on("then-end",function(t,e,n){this.nextPromise=n;var r=this.promise;a.emit("propagate",[r,!0],n)}),a.on("cb-end",function(t,e,n){a.emit("propagate",[n,!0],this.nextPromise)}),a.on("propagate",function(t,e,n){this.getCtx&&!e||(this.getCtx=function(){if(t instanceof Promise)var e=a.context(t);return e&&e.getCtx?e.getCtx():this})}),r.toString=function(){return""+f})},{}],12:[function(t,e,n){var r=t("ee").get("raf"),o=t(26)(r),i="equestAnimationFrame";e.exports=r,o.inPlace(window,["r"+i,"mozR"+i,"webkitR"+i,"msR"+i],"raf-"),r.on("raf-start",function(t){t[0]=o(t[0],"fn-")})},{}],13:[function(t,e,n){function r(t,e,n){t[0]=a(t[0],"fn-",null,n)}function o(t,e,n){this.method=n,this.timerDuration=isNaN(t[1])?0:+t[1],t[0]=a(t[0],"fn-",this,n)}var i=t("ee").get("timer"),a=t(26)(i),s="setTimeout",c="setInterval",f="clearTimeout",u="-start",d="-";e.exports=i,a.inPlace(window,[s,"setImmediate"],s+d),a.inPlace(window,[c],c+d),a.inPlace(window,[f,"clearImmediate"],f+d),i.on(c+u,r),i.on(s+u,o)},{}],14:[function(t,e,n){function r(t,e){d.inPlace(e,["onreadystatechange"],"fn-",s)}function o(){var t=this,e=u.context(t);t.readyState>3&&!e.resolved&&(e.resolved=!0,u.emit("xhr-resolved",[],t)),d.inPlace(t,y,"fn-",s)}function i(t){x.push(t),h&&(b?b.then(a):v?v(a):(E=-E,R.data=E))}function a(){for(var t=0;t<x.length;t++)r([],x[t]);x.length&&(x=[])}function s(t,e){return e}function c(t,e){for(var n in t)e[n]=t[n];return e}t(6);var f=t("ee"),u=f.get("xhr"),d=t(26)(u),p=NREUM.o,l=p.XHR,h=p.MO,m=p.PR,v=p.SI,w="readystatechange",y=["onload","onerror","onabort","onloadstart","onloadend","onprogress","ontimeout"],x=[];e.exports=u;var g=window.XMLHttpRequest=function(t){var e=new l(t);try{u.emit("new-xhr",[e],e),e.addEventListener(w,o,!1)}catch(n){try{u.emit("internal-error",[n])}catch(r){}}return e};if(c(l,g),g.prototype=l.prototype,d.inPlace(g.prototype,["open","send"],"-xhr-",s),u.on("send-xhr-start",function(t,e){r(t,e),i(e)}),u.on("open-xhr-start",r),h){var b=m&&m.resolve();if(!v&&!m){var E=1,R=document.createTextNode(E);new h(a).observe(R,{characterData:!0})}}else f.on("fn-end",function(t){t[0]&&t[0].type===w||a()})},{}],15:[function(t,e,n){function r(){var t=window.NREUM,e=t.info.accountID||null,n=t.info.agentID||null,r=t.info.trustKey||null,i="btoa"in window&&"function"==typeof window.btoa;if(!e||!n||!i)return null;var a={v:[0,1],d:{ty:"Browser",ac:e,ap:n,id:o.generateCatId(),tr:o.generateCatId(),ti:Date.now()}};return r&&e!==r&&(a.d.tk=r),btoa(JSON.stringify(a))}var o=t(21);e.exports={generateTraceHeader:r}},{}],16:[function(t,e,n){function r(t){var e=this.params,n=this.metrics;if(!this.ended){this.ended=!0;for(var r=0;r<l;r++)t.removeEventListener(p[r],this.listener,!1);e.aborted||(n.duration=s.now()-this.startTime,this.loadCaptureCalled||4!==t.readyState?null==e.status&&(e.status=0):a(this,t),n.cbTime=this.cbTime,d.emit("xhr-done",[t],t),c("xhr",[e,n,this.startTime]))}}function o(t,e){var n=t.responseType;if("json"===n&&null!==e)return e;var r="arraybuffer"===n||"blob"===n||"json"===n?t.response:t.responseText;return v(r)}function i(t,e){var n=f(e),r=t.params;r.host=n.hostname+":"+n.port,r.pathname=n.pathname,t.sameOrigin=n.sameOrigin}function a(t,e){t.params.status=e.status;var n=o(e,t.lastSize);if(n&&(t.metrics.rxSize=n),t.sameOrigin){var r=e.getResponseHeader("X-NewRelic-App-Data");r&&(t.params.cat=r.split(", ").pop())}t.loadCaptureCalled=!0}var s=t("loader");if(s.xhrWrappable){var c=t("handle"),f=t(17),u=t(15).generateTraceHeader,d=t("ee"),p=["load","error","abort","timeout"],l=p.length,h=t("id"),m=t(20),v=t(19),w=window.XMLHttpRequest;s.features.xhr=!0,t(14),d.on("new-xhr",function(t){var e=this;e.totalCbs=0,e.called=0,e.cbTime=0,e.end=r,e.ended=!1,e.xhrGuids={},e.lastSize=null,e.loadCaptureCalled=!1,t.addEventListener("load",function(n){a(e,t)},!1),m&&(m>34||m<10)||window.opera||t.addEventListener("progress",function(t){e.lastSize=t.loaded},!1)}),d.on("open-xhr-start",function(t){this.params={method:t[0]},i(this,t[1]),this.metrics={}}),d.on("open-xhr-end",function(t,e){"loader_config"in NREUM&&"xpid"in NREUM.loader_config&&this.sameOrigin&&e.setRequestHeader("X-NewRelic-ID",NREUM.loader_config.xpid);var n=!1;if("init"in NREUM&&"distributed_tracing"in NREUM.init&&(n=!!NREUM.init.distributed_tracing.enabled),n&&this.sameOrigin){var r=u();r&&e.setRequestHeader("newrelic",r)}}),d.on("send-xhr-start",function(t,e){var n=this.metrics,r=t[0],o=this;if(n&&r){var i=v(r);i&&(n.txSize=i)}this.startTime=s.now(),this.listener=function(t){try{"abort"!==t.type||o.loadCaptureCalled||(o.params.aborted=!0),("load"!==t.type||o.called===o.totalCbs&&(o.onloadCalled||"function"!=typeof e.onload))&&o.end(e)}catch(n){try{d.emit("internal-error",[n])}catch(r){}}};for(var a=0;a<l;a++)e.addEventListener(p[a],this.listener,!1)}),d.on("xhr-cb-time",function(t,e,n){this.cbTime+=t,e?this.onloadCalled=!0:this.called+=1,this.called!==this.totalCbs||!this.onloadCalled&&"function"==typeof n.onload||this.end(n)}),d.on("xhr-load-added",function(t,e){var n=""+h(t)+!!e;this.xhrGuids&&!this.xhrGuids[n]&&(this.xhrGuids[n]=!0,this.totalCbs+=1)}),d.on("xhr-load-removed",function(t,e){var n=""+h(t)+!!e;this.xhrGuids&&this.xhrGuids[n]&&(delete this.xhrGuids[n],this.totalCbs-=1)}),d.on("addEventListener-end",function(t,e){e instanceof w&&"load"===t[0]&&d.emit("xhr-load-added",[t[1],t[2]],e)}),d.on("removeEventListener-end",function(t,e){e instanceof w&&"load"===t[0]&&d.emit("xhr-load-removed",[t[1],t[2]],e)}),d.on("fn-start",function(t,e,n){e instanceof w&&("onload"===n&&(this.onload=!0),("load"===(t[0]&&t[0].type)||this.onload)&&(this.xhrCbStart=s.now()))}),d.on("fn-end",function(t,e){this.xhrCbStart&&d.emit("xhr-cb-time",[s.now()-this.xhrCbStart,this.onload,e],e)})}},{}],17:[function(t,e,n){e.exports=function(t){var e=document.createElement("a"),n=window.location,r={};e.href=t,r.port=e.port;var o=e.href.split("://");!r.port&&o[1]&&(r.port=o[1].split("/")[0].split("@").pop().split(":")[1]),r.port&&"0"!==r.port||(r.port="https"===o[0]?"443":"80"),r.hostname=e.hostname||n.hostname,r.pathname=e.pathname,r.protocol=o[0],"/"!==r.pathname.charAt(0)&&(r.pathname="/"+r.pathname);var i=!e.protocol||":"===e.protocol||e.protocol===n.protocol,a=e.hostname===document.domain&&e.port===n.port;return r.sameOrigin=i&&(!e.hostname||a),r}},{}],18:[function(t,e,n){function r(){}function o(t,e,n){return function(){return i(t,[f.now()].concat(s(arguments)),e?null:this,n),e?void 0:this}}var i=t("handle"),a=t(23),s=t(24),c=t("ee").get("tracer"),f=t("loader"),u=NREUM;"undefined"==typeof window.newrelic&&(newrelic=u);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],p="api-",l=p+"ixn-";a(d,function(t,e){u[e]=o(p+e,!0,"api")}),u.addPageAction=o(p+"addPageAction",!0),u.setCurrentRouteName=o(p+"routeName",!0),e.exports=newrelic,u.interaction=function(){return(new r).get()};var h=r.prototype={createTracer:function(t,e){var n={},r=this,o="function"==typeof e;return i(l+"tracer",[f.now(),t,n],r),function(){if(c.emit((o?"":"no-")+"fn-start",[f.now(),r,o],n),o)try{return e.apply(this,arguments)}catch(t){throw c.emit("fn-err",[arguments,this,t],n),t}finally{c.emit("fn-end",[f.now()],n)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,e){h[e]=o(l+e)}),newrelic.noticeError=function(t,e){"string"==typeof t&&(t=new Error(t)),i("err",[t,f.now(),!1,e])}},{}],19:[function(t,e,n){e.exports=function(t){if("string"==typeof t&&t.length)return t.length;if("object"==typeof t){if("undefined"!=typeof ArrayBuffer&&t instanceof ArrayBuffer&&t.byteLength)return t.byteLength;if("undefined"!=typeof Blob&&t instanceof Blob&&t.size)return t.size;if(!("undefined"!=typeof FormData&&t instanceof FormData))try{return JSON.stringify(t).length}catch(e){return}}}},{}],20:[function(t,e,n){var r=0,o=navigator.userAgent.match(/Firefox[\/\s](\d+\.\d+)/);o&&(r=+o[1]),e.exports=r},{}],21:[function(t,e,n){function r(){function t(){return e?15&e[n++]:16*Math.random()|0}var e=null,n=0,r=window.crypto||window.msCrypto;r&&r.getRandomValues&&(e=r.getRandomValues(new Uint8Array(31)));for(var o,i="xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx",a="",s=0;s<i.length;s++)o=i[s],"x"===o?a+=t().toString(16):"y"===o?(o=3&t()|8,a+=o.toString(16)):a+=o;return a}function o(){function t(){return e?15&e[n++]:16*Math.random()|0}var e=null,n=0,r=window.crypto||window.msCrypto;r&&r.getRandomValues&&Uint8Array&&(e=r.getRandomValues(new Uint8Array(31)));for(var o=[],i=0;i<16;i++)o.push(t().toString(16));return o.join("")}e.exports={generateUuid:r,generateCatId:o}},{}],22:[function(t,e,n){function r(t,e){if(!o)return!1;if(t!==o)return!1;if(!e)return!0;if(!i)return!1;for(var n=i.split("."),r=e.split("."),a=0;a<r.length;a++)if(r[a]!==n[a])return!1;return!0}var o=null,i=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var s=navigator.userAgent,c=s.match(a);c&&s.indexOf("Chrome")===-1&&s.indexOf("Chromium")===-1&&(o="Safari",i=c[1])}e.exports={agent:o,version:i,match:r}},{}],23:[function(t,e,n){function r(t,e){var n=[],r="",i=0;for(r in t)o.call(t,r)&&(n[i]=e(r,t[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;e.exports=r},{}],24:[function(t,e,n){function r(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,o=n-e||0,i=Array(o<0?0:o);++r<o;)i[r]=t[e+r];return i}e.exports=r},{}],25:[function(t,e,n){e.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],26:[function(t,e,n){function r(t){return!(t&&t instanceof Function&&t.apply&&!t[a])}var o=t("ee"),i=t(24),a="nr@original",s=Object.prototype.hasOwnProperty,c=!1;e.exports=function(t,e){function n(t,e,n,o){function nrWrapper(){var r,a,s,c;try{a=this,r=i(arguments),s="function"==typeof n?n(r,a):n||{}}catch(f){p([f,"",[r,a,o],s])}u(e+"start",[r,a,o],s);try{return c=t.apply(a,r)}catch(d){throw u(e+"err",[r,a,d],s),d}finally{u(e+"end",[r,a,c],s)}}return r(t)?t:(e||(e=""),nrWrapper[a]=t,d(t,nrWrapper),nrWrapper)}function f(t,e,o,i){o||(o="");var a,s,c,f="-"===o.charAt(0);for(c=0;c<e.length;c++)s=e[c],a=t[s],r(a)||(t[s]=n(a,f?s+o:o,i,s))}function u(n,r,o){if(!c||e){var i=c;c=!0;try{t.emit(n,r,o,e)}catch(a){p([a,n,r,o])}c=i}}function d(t,e){if(Object.defineProperty&&Object.keys)try{var n=Object.keys(t);return n.forEach(function(n){Object.defineProperty(e,n,{get:function(){return t[n]},set:function(e){return t[n]=e,e}})}),e}catch(r){p([r])}for(var o in t)s.call(t,o)&&(e[o]=t[o]);return e}function p(e){try{t.emit("internal-error",e)}catch(n){}}return t||(t=o),n.inPlace=f,n.flag=a,n}},{}],ee:[function(t,e,n){function r(){}function o(t){function e(t){return t&&t instanceof r?t:t?c(t,s,i):i()}function n(n,r,o,i){if(!p.aborted||i){t&&t(n,r,o);for(var a=e(o),s=m(n),c=s.length,f=0;f<c;f++)s[f].apply(a,r);var d=u[x[n]];return d&&d.push([g,n,r,a]),a}}function l(t,e){y[t]=m(t).concat(e)}function h(t,e){var n=y[t];if(n)for(var r=0;r<n.length;r++)n[r]===e&&n.splice(r,1)}function m(t){return y[t]||[]}function v(t){return d[t]=d[t]||o(n)}function w(t,e){f(t,function(t,n){e=e||"feature",x[n]=e,e in u||(u[e]=[])})}var y={},x={},g={on:l,addEventListener:l,removeEventListener:h,emit:n,get:v,listeners:m,context:e,buffer:w,abort:a,aborted:!1};return g}function i(){return new r}function a(){(u.api||u.feature)&&(p.aborted=!0,u=p.backlog={})}var s="nr@context",c=t("gos"),f=t(23),u={},d={},p=e.exports=o();p.backlog=u},{}],gos:[function(t,e,n){function r(t,e,n){if(o.call(t,e))return t[e];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return t[e]=r,r}var o=Object.prototype.hasOwnProperty;e.exports=r},{}],handle:[function(t,e,n){function r(t,e,n,r){o.buffer([t],r),o.emit(t,e,n)}var o=t("ee").get("handle");e.exports=r,r.ee=o},{}],id:[function(t,e,n){function r(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:a(t,i,function(){return o++})}var o=1,i="nr@id",a=t("gos");e.exports=r},{}],loader:[function(t,e,n){function r(){if(!E++){var t=b.info=NREUM.info,e=l.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(t&&t.licenseKey&&t.applicationID&&e))return u.abort();f(x,function(e,n){t[e]||(t[e]=n)}),c("mark",["onload",a()+b.offset],null,"api");var n=l.createElement("script");n.src="https://"+t.agent,e.parentNode.insertBefore(n,e)}}function o(){"complete"===l.readyState&&i()}function i(){c("mark",["domContent",a()+b.offset],null,"api")}function a(){return R.exists&&performance.now?Math.round(performance.now()):(s=Math.max((new Date).getTime(),s))-b.offset}var s=(new Date).getTime(),c=t("handle"),f=t(23),u=t("ee"),d=t(22),p=window,l=p.document,h="addEventListener",m="attachEvent",v=p.XMLHttpRequest,w=v&&v.prototype;NREUM.o={ST:setTimeout,SI:p.setImmediate,CT:clearTimeout,XHR:v,REQ:p.Request,EV:p.Event,PR:p.Promise,MO:p.MutationObserver};var y=""+location,x={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-spa-1130.min.js"},g=v&&w&&w[h]&&!/CriOS/.test(navigator.userAgent),b=e.exports={offset:s,now:a,origin:y,features:{},xhrWrappable:g,userAgent:d};t(18),l[h]?(l[h]("DOMContentLoaded",i,!1),p[h]("load",r,!1)):(l[m]("onreadystatechange",o),p[m]("onload",r)),c("mark",["firstbyte",s],null,"api");var E=0,R=t(25)},{}]},{},["loader",2,16,5,3,4]);</script>
    


<meta name="bb-env" content="production" />
<meta id="bb-canon-url" name="bb-canon-url" content="https://bitbucket.org">
<meta name="bb-api-canon-url" content="https://api.bitbucket.org">



<meta name="bb-commit-hash" content="b00b45df2659">
<meta name="bb-app-node" content="app-1115">
<meta name="bb-view-name" content="bitbucket.apps.repo2.views.SourceView">
<meta name="ignore-whitespace" content="False">
<meta name="tab-size" content="None">
<meta name="locale" content="en">
<meta name="application-name" content="Bitbucket">
<meta name="apple-mobile-web-app-title" content="Bitbucket">
<meta name="slack-app-id" content="A8W8QLZD1">
<meta name="statuspage-api-host" content="https://bqlf8qjztdtr.statuspage.io">


<meta name="theme-color" content="#0049B0">
<meta name="msapplication-TileColor" content="#0052CC">
<meta name="msapplication-TileImage" content="https://d301sr5gafysq2.cloudfront.net/b00b45df2659/img/logos/bitbucket/mstile-150x150.png">
<link rel="apple-touch-icon" sizes="180x180" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/b00b45df2659/img/logos/bitbucket/apple-touch-icon.png">
<link rel="icon" sizes="192x192" type="image/png" href="https://d301sr5gafysq2.cloudfront.net/b00b45df2659/img/logos/bitbucket/android-chrome-192x192.png">

<link rel="icon" sizes="16x16 24x24 32x32 64x64" type="image/x-icon" href="/favicon.ico?v=2">
<link rel="mask-icon" href="https://d301sr5gafysq2.cloudfront.net/b00b45df2659/img/logos/bitbucket/safari-pinned-tab.svg" color="#0052CC">

<link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="Bitbucket">

    <meta name="description" content="">
    <meta name="bb-single-page-app" content="true">
    
    <script nonce="npUVzrfhXAh4b0si">

if (window.performance) {

  
  window.performance.okayToSendMetrics = !document.hidden && 'onvisibilitychange' in document;

  if (window.performance.okayToSendMetrics) {

    
    window.addEventListener('visibilitychange', function () {
      if (document.hidden) {
        window.performance.okayToSendMetrics = false;
      }
    });
  }
}
</script>
    
    
      
        <link rel="stylesheet" href="https://d301sr5gafysq2.cloudfront.net/frontbucket/vendor.1e7fb924f5f8.css">
      
      
    
    
  </head>
  <body>
    <div id="root">
    
    </div>
    <script nonce="npUVzrfhXAh4b0si">
      window.__sentry__ = {"dsn": "https://2dcda83904474d8c86928ebbfa1ab294@sentry.io/1480772", "environment": "production"};
      window.__initial_state__ = {"section": {"repository": {"connectActions": [], "cloneProtocol": "https", "currentRepository": {"scm": "git", "website": "", "uuid": "{702780ae-56bc-46b1-a574-797350e3977d}", "links": {"clone": [{"href": "https://bitbucket.org/ibmi/opensource.git", "name": "https"}, {"href": "git@bitbucket.org:ibmi/opensource.git", "name": "ssh"}], "self": {"href": "https://bitbucket.org/!api/2.0/repositories/ibmi/opensource"}, "html": {"href": "https://bitbucket.org/ibmi/opensource"}, "avatar": {"href": "https://bytebucket.org/ravatar/%7B702780ae-56bc-46b1-a574-797350e3977d%7D?ts=1794105"}}, "name": "opensource", "project": {"uuid": "{0e0d785e-d0e8-487a-941c-b977a859de81}", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/teams/ibmi/projects/OP"}, "html": {"href": "https://bitbucket.org/account/user/ibmi/projects/OP"}, "avatar": {"href": "https://bitbucket.org/account/user/ibmi/projects/OP/avatar/32"}}, "name": "opensource", "created_on": "2016-06-01T13:37:25.881639+00:00", "key": "OP", "owner": {"username": "ibmi", "type": "team", "display_name": "IBM i", "uuid": "{43141185-5dfd-4722-a40b-2db793b061e1}", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/teams/%7B43141185-5dfd-4722-a40b-2db793b061e1%7D"}, "html": {"href": "https://bitbucket.org/%7B43141185-5dfd-4722-a40b-2db793b061e1%7D/"}, "avatar": {"href": "https://bitbucket.org/account/ibmi/avatar/"}}}, "updated_on": "2016-06-01T13:37:25.881674+00:00", "type": "project", "is_private": false, "description": null}, "language": "", "mainbranch": {"name": "master"}, "full_name": "ibmi/opensource", "owner": {"username": "ibmi", "display_name": "IBM i", "uuid": "{43141185-5dfd-4722-a40b-2db793b061e1}", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/teams/%7B43141185-5dfd-4722-a40b-2db793b061e1%7D"}, "html": {"href": "https://bitbucket.org/%7B43141185-5dfd-4722-a40b-2db793b061e1%7D/"}, "avatar": {"href": "https://bitbucket.org/account/ibmi/avatar/"}}, "created_on": "2016-04-01T17:16:04.731875+00:00", "type": "team", "properties": {}, "has_2fa_enabled": null}, "updated_on": "2019-10-22T09:27:05.447026+00:00", "type": "repository", "slug": "opensource", "is_private": false, "description": "The world of Open Source continues to rapidly evolve and change. This is also very much the case for the IBM i. Over the past few years we have seen an increased number of Open Source Technologies added to the IBM i. This page is intended to be a landing page to help you understand what is available today, how do you get access to it, as well as links to additional details and documentation."}, "mirrors": [], "menuItems": [{"analytics_label": "repository.source", "is_client_link": true, "icon_class": "icon-source", "badge_label": null, "weight": 200, "url": "/ibmi/opensource/src", "tab_name": "source", "can_display": true, "label": "Source", "type": "menu_item", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": ["/diff", "/history-node"], "target": "_self", "id": "repo-source-link", "icon": "icon-source"}, {"analytics_label": "repository.commits", "is_client_link": false, "icon_class": "icon-commits", "badge_label": null, "weight": 300, "url": "/ibmi/opensource/commits/", "tab_name": "commits", "can_display": true, "label": "Commits", "type": "menu_item", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": [], "target": "_self", "id": "repo-commits-link", "icon": "icon-commits"}, {"analytics_label": "repository.branches", "is_client_link": true, "icon_class": "icon-branches", "badge_label": null, "weight": 400, "url": "/ibmi/opensource/branches/", "tab_name": "branches", "can_display": true, "label": "Branches", "type": "menu_item", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": [], "target": "_self", "id": "repo-branches-link", "icon": "icon-branches"}, {"analytics_label": "repository.pullrequests", "is_client_link": true, "icon_class": "icon-pull-requests", "badge_label": null, "weight": 500, "url": "/ibmi/opensource/pull-requests/", "tab_name": "pullrequests", "can_display": true, "label": "Pull requests", "type": "menu_item", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": [], "target": "_self", "id": "repo-pullrequests-link", "icon": "icon-pull-requests"}, {"analytics_label": "user.addon", "is_client_link": true, "icon_class": "aui-iconfont-unfocus", "badge_label": null, "weight": 550, "url": "/ibmi/opensource/addon/pipelines/home", "tab_name": "repopage-oB98xE-add-on-link", "can_display": true, "label": "Pipelines", "icon_url": "https://bitbucket-connect-icons.s3.amazonaws.com/add-on/icons/62acf41d-386f-49fd-b823-4f86445390e2.svg?AWSAccessKeyId=AKIAIQWXW6WLXMB5QZAQ&Expires=1572966644&Signature=KB8llIqF5JwCVx2BvH5n1W2R8TA%3D", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": [], "type": "connect_menu_item", "id": "repopage-oB98xE-add-on-link", "target": "_self"}, {"analytics_label": "user.addon", "is_client_link": true, "icon_class": "aui-iconfont-unfocus", "badge_label": null, "weight": 560, "url": "/ibmi/opensource/addon/pipelines/deployments", "tab_name": "repopage-yaM899-add-on-link", "can_display": true, "label": "Deployments", "icon_url": "https://bitbucket-connect-icons.s3.amazonaws.com/add-on/icons/fca72d46-7e20-4dc4-b6a8-c83fb9665cc6.svg?AWSAccessKeyId=AKIAIQWXW6WLXMB5QZAQ&Expires=1572966644&Signature=sCIKClU0CHBwYTkx0nB%2BzsBaT8I%3D", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": [], "type": "connect_menu_item", "id": "repopage-yaM899-add-on-link", "target": "_self"}, {"analytics_label": "issues", "is_client_link": false, "icon_class": "icon-issues", "badge_label": "21 active issues", "weight": 600, "url": "/ibmi/opensource/issues?status=new&status=open", "tab_name": "issues", "can_display": true, "label": "Issues", "type": "menu_item", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": [], "target": "_self", "id": "repo-issues-link", "icon": "icon-issues"}, {"analytics_label": "repository.wiki", "is_client_link": false, "icon_class": "icon-wiki", "badge_label": null, "weight": 700, "url": "/ibmi/opensource/wiki/", "tab_name": "wiki", "can_display": true, "label": "Wiki", "type": "menu_item", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": [], "target": "_self", "id": "repo-wiki-link", "icon": "icon-wiki"}, {"analytics_label": "repository.downloads", "is_client_link": false, "icon_class": "icon-downloads", "badge_label": null, "weight": 800, "url": "/ibmi/opensource/downloads/", "tab_name": "downloads", "can_display": true, "label": "Downloads", "type": "menu_item", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": [], "target": "_self", "id": "repo-downloads-link", "icon": "icon-downloads"}], "bitbucketActions": [{"analytics_label": "repository.clone", "is_client_link": false, "icon_class": "icon-clone", "badge_label": null, "weight": 100, "url": "#clone", "tab_name": "clone", "can_display": true, "label": "<strong>Clone<\/strong> this repository", "type": "menu_item", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": [], "target": "_self", "id": "repo-clone-button", "icon": "icon-clone"}, {"analytics_label": "repository.compare", "is_client_link": false, "icon_class": "aui-icon-small aui-iconfont-devtools-compare", "badge_label": null, "weight": 400, "url": "/ibmi/opensource/branches/compare", "tab_name": "compare", "can_display": true, "label": "<strong>Compare<\/strong> branches or tags", "type": "menu_item", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": [], "target": "_self", "id": "repo-compare-link", "icon": "aui-icon-small aui-iconfont-devtools-compare"}, {"analytics_label": "repository.fork", "is_client_link": false, "icon_class": "icon-fork", "badge_label": null, "weight": 500, "url": "/ibmi/opensource/fork", "tab_name": "fork", "can_display": true, "label": "<strong>Fork<\/strong> this repository", "type": "menu_item", "anchor": true, "analytics_payload": {}, "matching_url_prefixes": [], "target": "_self", "id": "repo-fork-link", "icon": "icon-fork"}], "activeMenuItem": "source"}}, "global": {"needs_marketing_consent": false, "features": {"new-code-review-jira-contextual-view": true, "bleach-snippets-and-wikis": true, "statuspage-incident-message": true, "whitelisted_throttle_exemption": true, "use-elasticache-lsn-storage": true, "workspaces-api-proxy": true, "exp-share-to-invite-variation": false, "lxml-sanitization": true, "redirect-2fa-with-uuid": true, "atlassian-switcher": true, "account-switcher": true, "show-billing-errors": true, "restrict-commit-author-data": true, "nav-add-file": false, "markdown-and-linkify-commits": true, "crf-skip-excessive-diffs": true, "bbcs-367-log-session-invalidation": true, "unfiltered-repo-manager": true, "link-connect-issues": true, "ignore-bb-notification": true, "issue-tracker-progress-bar-calculator": true, "auto-prs-prime-fallback": true, "enable-boto3-s3-backend": true, "enable-router-metrics": true, "diff-api-renames": true, "clone-in-xcode": true, "create-pr-dialog": true, "container-menu-joinable-sites": true, "twofactor-recovery-flow": true, "provisioning-auto-login": true, "disable-jsonp": true, "use-moneybucket": true, "navigation-next": true, "frontbucket-leave-repository": true, "new-source-browser": true, "identity-passthrough-percent": true, "development-mode": true, "disable-prs-sidecar": true, "provisioning-api-list-teams": true, "hg-deprecation-warning": true}, "locale": "en", "geoip_country": null, "targetFeatures": {"new-code-review-jira-contextual-view": true, "bleach-snippets-and-wikis": true, "statuspage-incident-message": true, "whitelisted_throttle_exemption": true, "use-elasticache-lsn-storage": true, "pr-updates-endpoint-links": true, "ignore-bb-notification": true, "lxml-sanitization": true, "redirect-2fa-with-uuid": true, "atlassian-switcher": true, "account-switcher": true, "nav-next-notify-change": true, "disable-jsonp": true, "show-billing-errors": true, "show-guidance-message": true, "markdown-and-linkify-commits": true, "restrict-commit-author-data": true, "force-old-code-review": false, "access-controlled-dashboard": true, "markdown-embedded-html": false, "spa-profile-repository-list": true, "crf-skip-excessive-diffs": true, "bbcs-367-log-session-invalidation": true, "link-connect-issues": true, "clonebundles": true, "prlinks-installer": true, "pride-logo": false, "issue-tracker-progress-bar-calculator": true, "auto-prs-prime-fallback": true, "new-code-review-teams": true, "enable-boto3-s3-backend": true, "new-code-review-commits-api-usage": true, "workspaces-api-proxy": true, "diff-api-renames": true, "clone-in-xcode": true, "create-pr-dialog": true, "container-menu-joinable-sites": true, "twofactor-recovery-flow": true, "provisioning-auto-login": true, "unfiltered-repo-manager": true, "use-moneybucket": true, "navigation-next": true, "diff-api-20-refactor": true, "atlassian-editor": true, "frontbucket-leave-repository": true, "new-source-browser": true, "identity-passthrough-percent": true, "one-trust-integration": true, "exp-new-user-survey": true, "evolution": false, "new-code-review": false, "enable-router-metrics": true, "spa-commit-list": true, "disable-prs-sidecar": true, "development-mode": true, "provisioning-api-list-teams": true, "hg-deprecation-warning": true}, "isFocusedTask": false, "browser_monitoring": true, "identityPassthroughEnabled": true, "is_mobile_user_agent": false, "flags": [], "site_message": "", "isNavigationOpen": true, "path": "/ibmi/opensource/src/3e06f584c88b39f1964d2a58aa094a234a8f77d2/docs/porting/README.md", "focusedTaskBackButtonUrl": null, "targetUser": {"username": "ibmi", "display_name": "IBM i", "uuid": "{43141185-5dfd-4722-a40b-2db793b061e1}", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/teams/%7B43141185-5dfd-4722-a40b-2db793b061e1%7D"}, "html": {"href": "https://bitbucket.org/%7B43141185-5dfd-4722-a40b-2db793b061e1%7D/"}, "avatar": {"href": "https://bitbucket.org/account/ibmi/avatar/"}}, "created_on": "2016-04-01T17:16:04.731875+00:00", "type": "team", "properties": {}, "has_2fa_enabled": null}, "whats_new_feed": "https://bitbucket.org/blog/wp-json/wp/v2/posts?categories=196&context=embed&per_page=6&orderby=date&order=desc"}, "repository": {"source": {"section": {"hash": "3e06f584c88b39f1964d2a58aa094a234a8f77d2", "atRef": null, "ref": {"name": "master", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/repositories/ibmi/opensource/refs/branches/master"}, "html": {"href": "https://bitbucket.org/ibmi/opensource/branch/master"}}, "target": {"type": "commit", "hash": "3e06f584c88b39f1964d2a58aa094a234a8f77d2", "links": {"self": {"href": "https://bitbucket.org/!api/2.0/repositories/ibmi/opensource/commit/3e06f584c88b39f1964d2a58aa094a234a8f77d2"}, "html": {"href": "https://bitbucket.org/ibmi/opensource/commits/3e06f584c88b39f1964d2a58aa094a234a8f77d2"}}}}, "showCloneGuidance": false}}}};
      window.__settings__ = {"MARKETPLACE_TERMS_OF_USE_URL": null, "JIRA_ISSUE_COLLECTORS": {"code-review-beta": {"url": "https://bitbucketfeedback.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-4bqv2z/b/20/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=bb066400", "id": "bb066400"}, "jira-software-repo-page": {"url": "https://jira.atlassian.com/s/1ce410db1c7e1b043ed91ab8e28352e2-T/yl6d1c/804001/619f60e5de428c2ed7545f16096c303d/3.1.0/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-UK&collectorId=064d6699", "id": "064d6699"}, "code-review-rollout": {"url": "https://bitbucketfeedback.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-4bqv2z/b/20/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=de003e2d", "id": "de003e2d"}, "source-browser": {"url": "https://bitbucketfeedback.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-tqnsjm/b/20/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=c19c2ff6", "id": "c19c2ff6"}}, "CANON_URL": "https://bitbucket.org", "AVAILABLE_SITES_URL": "https://api-private.atlassian.com/available-sites", "CONSENT_HUB_FRONTEND_BASE_URL": "https://preferences.atlassian.com", "API_CANON_URL": "https://api.bitbucket.org", "SOCIAL_AUTH_ATLASSIANID_LOGOUT_URL": "https://id.atlassian.com/logout", "EMOJI_STANDARD_BASE_URL": "https://api-private.atlassian.com/emoji/"};
      window.__webpack_nonce__ = 'npUVzrfhXAh4b0si';
      window.isInitialLoadApdex = true;
    </script>
    
      <script nonce="npUVzrfhXAh4b0si">
        window.__webpack_public_path__ = 'https://d301sr5gafysq2.cloudfront.net/frontbucket/';
      </script>
      <script nonce="npUVzrfhXAh4b0si" src="https://d301sr5gafysq2.cloudfront.net/frontbucket/locales/en.bc66a8ccaf09fdfbd3ef.js"></script>
        <script nonce="npUVzrfhXAh4b0si" src="https://d301sr5gafysq2.cloudfront.net/frontbucket/runtime.158e2c73a1cca37f6dc4.js"></script>
        <script nonce="npUVzrfhXAh4b0si" src="https://d301sr5gafysq2.cloudfront.net/frontbucket/vendor.a32f7680068c7b1c7b31.js"></script>

      <!-- NOTE: we need ajs to load before app.js otherwise connect may not work -->
        <script nonce="npUVzrfhXAh4b0si" src="https://d301sr5gafysq2.cloudfront.net/frontbucket/ajs.cb1420734848b7617a89.js"></script>
      <script nonce="npUVzrfhXAh4b0si" src="https://d301sr5gafysq2.cloudfront.net/frontbucket/app.447ed0afc7a419b9590f.js"></script>
        <script nonce="npUVzrfhXAh4b0si" defer src="https://d301sr5gafysq2.cloudfront.net/frontbucket/performance-timing.9c30618348ba864c85f4.js"></script>
    
    <script nonce="npUVzrfhXAh4b0si" type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","queueTime":0,"licenseKey":"a2cef8c3d3","agent":"","transactionName":"NFcGYEdUW0IAVE1QCw0dIkFbVkFYDlkWWw0XUBFXXlBBHwBHSUpKEVcUWwcbQ1gEQEoDAgpeAVZHWkJCBA==","applicationID":"1841284","errorBeacon":"bam.nr-data.net","applicationTime":267}</script>
  </body>
</html>
# Getting support
If you have any questions or feedback, **contact [support](mailto:support@zenhub.com) for support**. The ZenHub Board for this repo has been sunset - all support inquiries should be submitted directly through the email above.

You can also submit a feature request [here](https://portal.productboard.com/zenhub).

# Overview

[Overview](#overview)

- [Root Endpoint](#root-endpoint)
- [Authentication](#authentication)
- [API Rate Limit](#api-rate-limit)
- [Errors](#errors)

[Endpoint Reference](#endpoint-reference)

- [Issues](#issues)
  - [Get Issue Data](#get-issue-data)
  - [Get Issue Events](#get-issue-events)
  - [Move an Issue Between Pipelines](#move-an-issue-between-pipelines)
  - [Set Issue Estimate](#set-issue-estimate)
- [Epics](#epics)
  - [Get Epics for a Repository](#get-epics-for-a-repository)
  - [Get Epic Data](#get-epic-data)
  - [Convert an Epic to an Issue](#convert-an-epic-to-an-issue)
  - [Convert an Issue to Epic](#convert-issue-to-epic)
  - [Add or Remove Issues from an Epic](#add-or-remove-issues-to-epic)
- [Board](#board)
  - [Get Board Data for a Repository](#get-the-zenhub-board-data-for-a-repository)
- [Milestones](#milestones)
  - [Set the Milestone Start Date](#set-milestone-start-date)
  - [Get the Milestone Start Date](#get-milestone-start-date)
- [Dependencies](#dependencies)
  - [Get Dependencies for a Repository](#get-dependencies-for-a-repository)
  - [Create a Dependency](#create-a-dependency)
  - [Remove a Dependency](#remove-a-dependency)
- [Release Reports](#release-reports)
  - [Create a Release Report](#create-a-release-report)
  - [Get a Release Report](#get-a-release-report)
  - [Get Release Reports for a Repository](#get-release-reports-for-a-repository)
  - [Edit a Release Report](#edit-a-release-report)
  - [Add a Repository to a Release Report](#add-a-repository-to-a-release-report)
  - [Remove a Repository from a Release Report](#remove-a-repository-from-a-release-report)
- [Release Report Issues](#release-report-issues)
  - [Get all the Issues in a Release Report](#get-all-the-issues-for-a-release-report)
  - [Add or Remove Issues from a Release Report](#add-or-remove-issues-to-or-from-a-release-report)

[Webhooks](#webhooks)

- [Custom Webhooks](#custom-webhooks)

[Contact Us](#contact-us)

## Root Endpoint

On Cloud, the root endpoint for the public API is `https://api.zenhub.io/`.
For ZenHub Enterprise, the root endpoint is `https://<zenhub_enterprise_host>/`.

## Authentication

All requests to the API need an API token. Generate a token in the **API Tokens** section of your ZenHub [Dashboard](https://app.zenhub.com/dashboard/tokens) (or `https://<zenhub_enterprise_host>/app/dashboard/tokens` for ZenHub Enterprise). The token is sent in the `X-Authentication-Token` header. For example, using `curl` it’d be:

```sh
curl -H 'X-Authentication-Token: TOKEN' URL
```

Alternatively, you can choose to send the token in the URL using the `access_token` query string attribute. To do so, add `?access_token=TOKEN` to any URL.

#### Notes

- Each user may only have one token, so generating a new token will invalidate previously created tokens.
- For ZenHub Enterprise users, please follow the instructions in `https://<zenhub_enterprise_host>/setup/howto/api`

## API Rate Limit

We allow a maximum of 100 requests per minute to our API. All requests responses include the following headers related to this limitation.

Header | Description
------ | -------
`X-RateLimit-Limit` | Total number of requests allowed before the reset time
`X-RateLimit-Used` | Number of requests sent in the current cycle. Will be set to 0 at the reset time.
`X-RateLimit-Reset` | Time in UTC epoch seconds when the usage gets reset.

To avoid time differences between your computer and our servers, we suggest to use the `Date` header in the response to know exactly when the limit is reset.

## Errors

The ZenHub API can return the following errors:

Status Code | Description
----------- | -------
`401` | The token is not valid. See [Authentication](#authentication).
`403` | Reached request limit to the API. See [API Limits](#api-rate-limits).
`404` | Not found.

# Endpoint Reference

#### Notes

- `repo_id` is the ID of the repository, not its full name. For example, the ID of the `ZenHubIO/API` repository is `47655910`. To find out the ID of your repository, use [GitHub’s API](https://developer.github.com/v3/repos/#get), or copy it from the URL of the Board (for this repo, the Board URL is https://github.com/ZenHubIO/API#boards?repos=47655910).

## Issues

- [Get Issue Data](#get-issue-data)
- [Get Issue Events](#get-issue-events)
- [Move an Issue Between Pipelines](#move-an-issue-between-pipelines)
- [Set Issue Estimate](#set-issue-estimate)

### Get Issue Data

Get the data for a specific issue.

#### Endpoint

`GET /p1/repositories/:repo_id/issues/:issue_number`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required
|`issue_number`|`Number`|Required

#### Example Response

```json
{
  "estimate": {
    "value": 8
  },
  "plus_ones": [
    {
      "created_at": "2015-12-11T18:43:22.296Z"
    }
  ],
  "pipeline": {
    "name": "In Progress"
  },
  "is_epic": true
}
```

#### Notes

- `plus_ones[].user_id` was removed from the response.
- Closed issues might take up to one minute to show up in the Closed Pipeline. Similarly, reopened issues might take up to one minute to show in the correct Pipeline.

### Get Issue Events

Get the events for an issue.

#### Endpoint

`GET /p1/repositories/:repo_id/issues/:issue_number/events`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required
|`issue_number`|`Number`|Required


#### Example Response

```json
[
  {
    "user_id": 16717,
    "type": "estimateIssue",
    "created_at": "2015-12-11T19:43:22.296Z",
    "from_estimate": {
      "value": 8
    }
  },
  {
    "user_id": 16717,
    "type": "estimateIssue",
    "created_at": "2015-12-11T18:43:22.296Z",
    "from_estimate": {
      "value": 4
    },
    "to_estimate": {
      "value": 8
    }
  },
  {
    "user_id": 16717,
    "type": "estimateIssue",
    "created_at": "2015-12-11T13:43:22.296Z",
    "to_estimate": {
      "value": 4
    }
  },
  {
    "user_id": 16717,
    "type": "transferIssue",
    "created_at": "2015-12-11T12:43:22.296Z",
    "from_pipeline": {
      "name": "Backlog"
    },
    "to_pipeline": {
      "name": "In progress"
    }
  },
  {
    "user_id": 16717,
    "type": "transferIssue",
    "created_at": "2015-12-11T11:43:22.296Z",
    "to_pipeline": {
      "name": "Backlog"
    }
  }
]
```

#### Notes

- Returns issue events, sorted by creation time, most recent first.
- Each event contains the _User ID_ of the user who performed the change, the _Creation Date_ of the event, and the event _Type_.
- Type can be either `estimateIssue` or `transferIssue`. The values before and after the event are included in the event data.

### Move an Issue Between Pipelines

Moves an issue between the Pipelines in your repository.

#### Endpoint

`POST /p1/repositories/:repo_id/issues/:issue_number/moves`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required
|`issue_number`|`Number`|Required

#### Body Parameters

|Name|Type|Comments
------------ | ------ | -------
|`pipeline_id`|`String`|Required
|`position`|`String` or `Number`| Required

#### Notes

- `pipeline_id` is the ID for one of the Pipelines in your repository (i.e: In Progress, Done, QA). In order to obtain this ID, you can use the [_Get Board Data for a Repository_](#get-the-zenhub-board-data-for-a-repository) endpoint.
- `position` can be specified as `top` or `bottom`, or a `0`-based position in the Pipeline such as `1`, which would be the second position in the Pipeline.

#### Example Request Body

```json
{
  "pipeline_id": "58bf13aba426771426665e60",
  "position": "top"
}
```

#### Example Response

Status `200` for a successful move. No response body.

### Set Issue Estimate

#### Endpoint

`PUT /p1/repositories/:repo_id/issues/:issue_number/estimate`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required
|`issue_number`|`Number`|Required

#### Body Parameters

|Name|Type|Comments
------------ | ------ | -------
|`estimate`|`Number`|Required, number representing estimate value

#### Example Request

```json
{ "estimate": 15 }
```

#### Example Response

```json
{ "estimate": 15 }
```

## Epics

- [Get Epics for a Repository](#get-epics-for-a-repository)
- [Get Epic Data](#get-epic-data)
- [Convert an Epic to an Issue](#convert-an-epic-to-an-issue)
- [Convert an Issue to Epic](#convert-issue-to-epic)
- [Add or Remove Issues from an Epic](#add-or-remove-issues-to-epic)

### Get Epics for a repository

Get all Epics for a repository

#### Endpoint

`GET /p1/repositories/:repo_id/epics`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required

#### Example Response

```json
{
  "epic_issues": [
    {
      "issue_number": 3953,
      "repo_id": 1234567,
      "issue_url": "https://github.com/RepoOwner/RepoName/issues/3953"
    },
    {
      "issue_number": 1342,
      "repo_id": 1234567,
      "issue_url": "https://github.com/RepoOwner/RepoName/issues/1342"
    },
  ]
}
```

#### Notes

- The endpoint returns an array of the repository’s Epics. The issue number, repository ID,
and GitHub issue URL is provided for each Epic.
- If an issue is only an issue belonging to an Epic (and not a parent Epic), it is not considered an Epic and won’t be included in the return array.

### Get Epic Data

Get the data for an Epic issue.

#### Endpoint

`GET /p1/repositories/:repo_id/epics/:epic_id`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required
|`epic_id`|`Number`|Required, Github issue number

#### Notes

- `epic_id` is the GitHub issue number. You may fetch the list of Epics using `Get Epics for a repository` endpoint.

#### Example Response
```json
{
  "total_epic_estimates": { "value": 60 },
  "estimate": { "value": 10 },
  "pipeline": { "name": "Backlog" },
  "issues": [
    {
      "issue_number": 3161,
      "is_epic": true,
      "repo_id": 1099029,
      "estimate": { "value": 40 },
      "pipeline": { "name": "New Issues" }
    },
    {
      "issue_number": 2,
      "is_epic": false,
      "repo_id": 1234567,
      "estimate": { "value": 10 },
      "pipeline": { "name": "New Issues" }
    },
    {
      "issue_number": 1,
      "is_epic": false,
      "repo_id": 1234567
    },
    {
      "issue_number": 6,
      "is_epic": false,
      "repo_id": 1234567
    },
    {
      "issue_number": 7,
      "is_epic": true,
      "repo_id": 9876543
    }
  ]
}
```

#### Notes

The endpoint returns:

- the total Epic Estimate value (the sum of all the Estimates of Issues contained within
  the Epic, as well as the Estimate of the Epic itself)
- the Estimate of the Epic
- the name of the Pipeline the Epic is in
- issues belonging to the Epic

For each issue belonging to the Epic:

- issue number
- repo ID
- Estimate value
- `is_epic` flag (`true` or `false`)
- if the issue is from the same repository as the Epic, the ZenHub Board’s Pipeline name (from the repo the Epic is in) is attached.

### Convert an Epic to an Issue

Converts an Epic back to a regular issue.

#### Endpoint

`POST /p1/repositories/:repo_id/epics/:issue_number/convert_to_issue`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required
|`issue_number`|`Number`|Required, the number of the issue to be converted

#### Example Response

- `200` if the issue was converted to Epic successfully

Does not return any body in the response.

### Convert Issue to Epic

Converts an issue to an Epic, along with any issues that should be part of it.

#### Endpoint

`POST /p1/repositories/:repo_id/issues/:issue_number/convert_to_epic`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required
|`issue_number`|`Number`|Required

#### Body Parameters

|Name|Type|Comments
------------ | ------ | -------
|`issues`|`[{repo_id: Number, issue_number: Number}]`|Required, array of Objects with `repo_id` and `issue_number`

#### Example Request Body

```json
{
  "issues": [
    { "repo_id": 13550592, "issue_number": 3 },
    { "repo_id": 13550592, "issue_number": 1 }
  ]
}
```

#### Response

Does not return any body in the response.

- `200` if the issue was converted to Epic successfully
- `400` if the supplied issue is already an Epic

### Add or remove issues to Epic

Bulk add or remove issues to an Epic. The result returns which issue was added or removed from the Epic.

#### Endpoint

`POST /p1/repositories/:repo_id/epics/:issue_number/update_issues`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required
|`issue_number`|`Number`|Required

#### Body Parameters

|Name|Type|Comments
------------ | ------ | -------
|`remove_issues`|[`{repo_id: Number, issue_number: Number}`]|Required, array of Objects with `repo_id` and `issue_number`
|`add_issues`|[`{repo_id: Number, issue_number: Number}`]|Required, array of Objects with `repo_id` and `issue_number`

#### Example Request Body

```json
{
  "remove_issues": [
    { "repo_id": 13550592, "issue_number": 3 }
  ],
  "add_issues": [
    { "repo_id": 13550592, "issue_number": 2 },
    { "repo_id": 13550592, "issue_number": 1 }
  ]
}
```

#### Notes

- `remove_issues` is an array that indicates with issues we want to remove from the specified Epic. They should be specified as an array containing objects with the issue’s `repo_id` and `issue_number`.
- `add_issues` is an array that indicates with issues we want to add to the specified Epic. They should be specified as an array containing objects with the issue’s `repo_id` and `issue_number`.

#### Example Response

```json
{
  "removed_issues":[
    { "repo_id": 3887883, "issue_number": 3 }
  ],
  "added_issues":[
    { "repo_id": 3887883, "issue_number": 2 },
    { "repo_id": 3887883, "issue_number": 1 }
  ]
}
```

#### Notes

- `removed_issues` shows which issues were removed in this operation.
- `add_issues` shows which issues were added in this operation.
- Returns a `404` if the Epic doesn’t exist

## Board

- [Get Board Data for a Repository](#get-the-zenhub-board-data-for-a-repository)

### Get the ZenHub Board data for a repository

#### Endpoint

`GET /p1/repositories/:repo_id/board`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required

#### Example Response

```json
{
  "pipelines": [
    {
      "id": "595d430add03f01d32460080",
      "name": "New Issues",
      "issues": [
        {
          "issue_number": 279,
          "estimate": { "value": 40 },
          "position": 0,
          "is_epic": true
        },
        {
          "issue_number": 142,
          "is_epic": false
        }
      ]
    },
    {
      "id": "595d430add03f01d32460081",
      "name": "Backlog",
      "issues": [
        {
          "issue_number": 303,
          "estimate": { "value": 40 },
          "position": 3,
          "is_epic": false
        }
      ]
    },
    {
      "id": "595d430add03f01d32460082",
      "name": "To Do",
      "issues": [
        {
          "issue_number": 380,
          "estimate": { "value": 1 },
          "position": 0,
          "is_epic": true
        },
        {
          "issue_number": 284,
          "position": 2,
          "is_epic": false
        },
        {
          "issue_number": 329,
          "estimate": { "value": 8 },
          "position": 7,
          "is_epic": false
        }
      ]
    }
  ]
}
```

#### Notes

- The endpoint returns the Board’s pipelines, plus the issues contained within each Pipeline. It returns the issue number of each issue, their position in the Board, the `is_epic` flag (`true` or `false`), and its Estimate (if set).
- Even if the issues are returned in the right order, the position can’t be guessed from its index. Note that some issues won’t have position – this is because they have not been prioritized on your Board.
- The Board returned by the endpoint doesn’t include closed issues. To get closed issues for a repository, you can use the GitHub API. Reopened issues might take up to one minute to appear in the correct Pipeline.

## Milestones

- [Set the Milestone Start Date](#set-milestone-start-date)
- [Get the Milestone Start Date](#get-milestone-start-date)

### Set milestone start date

#### Endpoint

`POST /p1/repositories/:repo_id/milestones/:milestone_number/start_date`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required
|`milestone_number`|`Number`|Required

#### Body Parameters

|Name|Type|Comments
------------ | ------ | -------
|`start_date`| ISO8601 date string|Required

#### Example Request Body

```json
{ "start_date": "2010-11-13T01:38:56.842Z" }
```

#### Example Response

```json
{ "start_date": "2010-11-13T01:38:56.842Z" }
```

### Get milestone start date

#### Endpoint

`GET /p1/repositories/:repo_id/milestones/:milestone_number/start_date`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required
|`milestone_number`|`Number`|Required

#### Example Response

```json
{ "start_date": "2010-11-13T01:38:56.842Z" }
```

## Dependencies

- [Get Dependencies for a Repository](#get-dependencies-for-a-repository)
- [Create a Dependency](#create-a-dependency)
- [Remove a Dependency](#remove-a-dependency)

### Get Dependencies for a Repository

#### Endpoint

`GET /p1/repositories/:repo_id/dependencies`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required

#### Example Response

```json
{
  "dependencies": [
    {
      "blocking": {
        "issue_number": 3953,
        "repo_id": 1234567
      },
      "blocked": {
        "issue_number": 1342,
        "repo_id": 1234567
      }
    },
    {
      "blocking": {
        "issue_number": 5,
        "repo_id":  987
      },
      "blocked": {
        "issue_number": 1342,
        "repo_id": 1234567
      }
    },
  ]
}
```

#### Notes

- This endpoint fetches all dependencies associated to the given repository that the user has read permission to
- The endpoint takes a `repo_id` param in the URL.
- Only dependencies where the user has read permissions to both sides will be returned

## Create a Dependency

#### Endpoint

`POST /p1/dependencies`

#### Body Parameters

|Name|Type|Comments
------------ | ------ | -------
|`blocking`|`Object`| Required
|`blocking.repo_id`| `Number`| Required
|`blocking.issue_number`| `Number`| Required
|`blocked`|`Object`| Required
|`blocked.repo_id`| `Number`| Required
|`blocked.issue_number`| `Number`| Required

#### Example Request Body
```json
{
  "blocking": {
    "repo_id": 92563409,
    "issue_number": 14
  },
  "blocked": {
    "repo_id": 92563409,
    "issue_number": 13
  }
}
```

#### Example Response Body
```json
{
  "blocking": {
    "repo_id": 92563409,
    "issue_number": 14
  },
  "blocked": {
    "repo_id": 92563409,
    "issue_number": 13
  }
}
```

#### Notes

- This endpoint creates one dependency
- The endpoint takes a `dependency` in the Body (see description above).
- User needs write permission on both repositories
- Cannot create dependency that will cause cycle, or between repositories not in the same workspace
- On success: returns HTTP 200 and returns the created object

## Remove a Dependency

#### Endpoint

`DELETE /p1/dependencies`

#### Body Parameters

|Name|Type|Comments
------------ | ------ | -------
|`blocking`|`Object`| Required
|`blocking.repo_id`| `Number`| Required
|`blocking.issue_number`| `Number`| Required
|`blocked`|`Object`| Required
|`blocked.repo_id`| `Number`| Required
|`blocked.issue_number`| `Number`| Required

#### Example Request Body
```json
{
  "blocking": {
    "repo_id": 92563409,
    "issue_number": 14
  },
  "blocked": {
    "repo_id": 92563409,
    "issue_number": 13
  }
}
```

#### Notes

- This endpoint removes one dependency
- The endpoint takes a `dependency` in the Body (see description above).
- User needs write permission on both repositories
- On success: returns HTTP 204 No Content and empty body

## Release Reports

- [Create a Release Report](#create-a-release-report)
- [Get a Release Report](#get-a-release-report)
- [Get Release Reports for a Repository](#get-release-reports-for-a-repository)
- [Edit a Release Report](#edit-a-release-report)
- [Add a Repository to a Release Report](#add-a-repository-to-a-release-report)
- [Remove a Repository from a Release Report](#remove-a-repository-from-a-release-report)

### Create a Release Report

#### Endpoint

`POST /p1/repositories/:repo_id/reports/release`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required

#### Body Parameters

|Name|Type|Comments
------------ | ------ | -------
|`title`|`String`|Required
|`description`| `String`| Optional
|`start_date`| ISO8601 date string| Required
|`desired_end_date`| ISO8601 date string| Required
|`repositories`| `[Number]`| Optional

#### Example Request Body
```json
{
  "title": "Great title",
  "description": "Amazing description",
  "start_date": "2007-01-01T00:00:00Z",
  "desired_end_date": "2007-01-01T00:00:00Z",
  "repositories": [
    103707262
  ]
}
```

#### Example Response

```json
{
  "release_id": "59dff4f508399a35a276a1ea",
  "title": "Great title",
  "description": "Amazing description",
  "start_date": "2007-01-01T00:00:00.000Z",
  "desired_end_date": "2007-01-01T00:00:00.000Z",
  "created_at": "2017-10-12T23:04:21.795Z",
  "closed_at": null,
  "state": "open",
  "repositories": [
    103707262
  ]
}
```

#### Notes

- **CHANGE NOTICE:** Only the repositories provided in the param and the body will be added to the Release Report - change in effect mid February 2019.
- The endpoint takes a `repo_id` param in the URL.
- Additional repository IDs can be passed in the body `repositories` parameter
- Any Boards not associated with the URL `repo_id` parameter, but associated with repositories in the request body `repositories` parameter will also be associated to the Release Report.
- The user creating the release requires push permission to the repositories in the request.

### Get a Release Report

#### Endpoint

`GET /p1/reports/release/:release_id`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`release_id`|`String`|Required

#### Example Response

```json
{
  "release_id": "59d3cd520a430a6344fd3bdb",
  "title": "Test release",
  "description": "",
  "start_date": "2017-10-01T19:00:00.000Z",
  "desired_end_date": "2017-10-03T19:00:00.000Z",
  "created_at": "2017-10-03T17:48:02.701Z",
  "closed_at": null,
  "state": "open",
  "repositories": [
    105683718
  ]
}
```

### Get Release Reports for a Repository

#### Endpoint

`GET /p1/repositories/:repo_id/reports/releases`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`repo_id`|`Number`|Required

#### Example Response

```json
[
  {
    "release_id": "59cbf2fde010f7a5207406e8",
    "title": "Great title for release 1",
    "description": "Great description for release",
    "start_date": "2000-10-10T00:00:00.000Z",
    "desired_end_date": "2010-10-10T00:00:00.000Z",
    "created_at": "2017-09-27T18:50:37.418Z",
    "closed_at": null,
    "state": "open"
  },
  {
    "release_id": "59cbf2fde010f7a5207406e8",
    "title": "Great title for release 2",
    "description": "Great description for release",
    "start_date": "2000-10-10T00:00:00.000Z",
    "desired_end_date": "2010-10-10T00:00:00.000Z",
    "created_at": "2017-09-27T18:50:37.418Z",
    "closed_at": null,
    "state": "open"
  }
]
```

### Edit a Release Report

#### Endpoint

`PATCH /p1/reports/release/:release_id`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`release_id`|`String`|Required

#### Body Parameters

|Name|Type|Comments
------------ | ------ | -------
|`title`|`String`|Required
|`description`|`String`|Optional
|`start_date`|ISO8601 date string|Optional
|`desired_end_date`| ISO8601 date string| Optional
|`state`| `String`|Optional, `open` or `closed`

#### Example Request Body

```json
{
  "title": "Amazing title",
  "description": "Amazing description",
  "start_date": "2007-01-01T00:00:00Z",
  "desired_end_date": "2007-01-01T00:00:00Z",
  "state": "closed"
}
```

#### Example Response

```json
{
  "release_id": "59d3d6438b3f16667f9e7174",
  "title": "Amazing title",
  "description": "Amazing description",
  "start_date": "2007-01-01T00:00:00.000Z",
  "desired_end_date": "2007-01-01T00:00:00.000Z",
  "created_at": "2017-10-03T18:26:11.700Z",
  "closed_at": "2017-10-03T18:26:11.700Z",
  "state": "closed",
  "repositories": [
    105683567,
    105683718
  ]
}
```

### Add a Repository to a Release Report
#### Endpoint

`POST /p1/reports/release/:release_id/repository/:repo_id`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`release_id`|`String`|Required
|`repo_id`|`Number`|Required


#### Notes
- On success, returns HTTP 200 OK and empty body


### Remove a Repository from a Release Report
#### Endpoint

`DELETE /p1/reports/release/:release_id/repository/:repo_id`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`release_id`|`String`|Required
|`repo_id`|`Number`|Required


#### Notes
- On success, returns HTTP 204 OK and empty body


### Add Workspaces to a Release Report
#### **DEPRECATED**
#### Endpoint
`PATCH /p1/reports/release/:release_id/workspaces/add`


### Remove Workspaces from Release Report
#### **DEPRECATED**
#### Endpoint
`PATCH /p1/reports/release/:release_id/workspaces/remove`

## Release Report Issues

- [Get all the Issues in a Release Report](#get-all-the-issues-for-a-release-report)
- [Add or Remove Issues from a Release Report](#add-or-remove-issues-to-or-from-a-release-report)

### Get all the Issues for a Release Report

#### Endpoint

`GET /p1/reports/release/:release_id/issues`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`release_id`|`String`|Required

#### Example Response

```json
[
  { "repo_id": 103707262, "issue_number": 2 },
  { "repo_id": 103707262, "issue_number": 3 }
]
```

### Add or Remove Issues to or from a Release Report

#### Endpoint

`PATCH /p1/reports/release/:release_id/issues`

#### URL Parameters

|Name|Type|Comments
------------ | ------ | -------
|`release_id`|`String`|Required

#### Body Parameters

|Name|Type|Comments
------------ | ------ | -------
|`add_issues`|`[{repo_id: Number, issue_number: Number}]`|Required, array of Objects with `repo_id` and `issue_number`
|`remove_issues`|`[{repo_id: Number, issue_number: Number}]`|Required, array of Objects with `repo_id` and `issue_number`

#### Note

- Both the `add_issues` and `remove_issues` keys are required, but can be an empty array when not used

#### Example Body Request

```json
{
  "add_issues": [
    { "repo_id": 103707262, "issue_number": 3 }
  ],
  "remove_issues": []
}
```

#### Example Response

```json
{
  "added": [
    { "repo_id": 103707262, "issue_number": 3 }
  ],
  "removed": []
}
```

#### Note

- Adding and removing issues can be done in the same request by populating with the `add_issues` and `remove_issues` keys.

### Webhooks

You can use our webhooks to fetch or store your ZenHub data, in real time, across services like Slack, Gitter, Spark, HipChat, or something custom!

To set up an integration, head on over to our [Dashboard](https://app.zenhub.com/dashboard), navigate to your organization, and select the **Slack & Integrations** tab. From there, you may choose one of the 5 services (Slack, HipChat, Gitter, Spark, or Custom).

For instructions, you'll notice the `How to create a webhook` link changes dynamically based on the service you select. Simply choose a repository with which to connect, add an optional description, paste your webhook, and click "Add" to save your new integration.

<img src="https://cloud.githubusercontent.com/assets/8771909/18925366/937133e2-8568-11e6-9f6a-da09edd63d16.jpg" alt="ZenHub integrations">

### Custom webhooks

Our custom webhook sends a POST request to your webhook for multiple events that occur on your ZenHub board:

#### Issue transfer

```json
{
  "type": "issue_transfer",
  "github_url": "https://github.com/ZenHubIO/support/issues/618",
  "organization": "ZenHubHQ",
  "repo": "support",
  "user_name": "ZenHubIO",
  "issue_number": "618",
  "issue_title": "ZenHub Change Log",
  "to_pipeline_name": "New Issues",
  "from_pipeline_name": "Discussion"
}
```

#### Estimate Set

```json
{
  "type": "estimate_set",
  "github_url": "https://github.com/ZenHubIO/support/issues/618",
  "organization": "ZenHubHQ",
  "repo": "support",
  "user_name": "ZenHubIO",
  "issue_number": "618",
  "issue_title": "ZenHub Change Log",
  "estimate": "8"
}
```

#### Estimate Cleared

```json
{
  "type": "estimate_cleared",
  "github_url": "https://github.com/ZenHubIO/support/issues/618",
  "organization": "ZenHubHQ",
  "repo": "support",
  "user_name": "ZenHubIO",
  "issue_number": "618",
  "issue_title": "ZenHub Change Log",
}
```

#### Issue Reprioritized

```json
{
  "type": "issue_reprioritized",
  "github_url": "https://github.com/ZenHubIO/support/issues/618",
  "organization": "ZenHubHQ",
  "repo": "support",
  "user_name": "ZenHubIO",
  "issue_number": "618",
  "issue_title": "ZenHub Change Log",
  "to_pipeline_name": "Backlog",
  "from_position": "4",
  "to_position": "0"
}
```

As an example, here's a simple Node/Express app that would be able receive the webhooks (using ngrok):

```javascript
var express = require('express');
var http = require('http');
var bodyParser = require('body-parser');
var app = express();

http.createServer(app).listen('6000', function() {
  console.log('Listening on 6000');
});

app.use(bodyParser());

app.post('*', function(req, res) {
  console.dir(req.body);
});
```

# Contact us

We’d love to hear from you. If you have any questions, concerns, or ideas related to the ZenHub API, open an issue in our [Support repo](https://github.com/ZenHubIO/support/issues#boards) or find us on [Twitter](http://www.twitter.com/ZenHubHQ).
